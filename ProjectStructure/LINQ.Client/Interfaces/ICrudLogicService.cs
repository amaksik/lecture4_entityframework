﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Client.Interfaces
{
    interface ICrudLogicService
    {
        Task Create(string model, Type type);
        Task<IEnumerable<Type>> Get(Type type);
        Task Update(string model, Type type);
        Task Delete(int id, Type type);
    }
}
