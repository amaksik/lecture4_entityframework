﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectStructure.Common.Models.DTO;

namespace ProjectStructure.BL.Interfaces
{
    public interface IUserService 
    {
        IEnumerable<UserDTO> GetAllUsers();

        void CreateUser(UserDTO user);
        void UpdateUser(UserDTO user);
        void DeleteUser(int userId);
    }
}
