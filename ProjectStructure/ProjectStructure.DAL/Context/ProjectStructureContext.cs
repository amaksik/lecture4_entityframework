﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Models;

namespace ProjectStructure.DAL.Context
{
    public class ProjectStructureContext : DbContext
    {
        public ProjectStructureContext(DbContextOptions<ProjectStructureContext> options) : base(options)
        {
        }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var projects = new List<Project>
            {
                new Project{
                    Id = 1, 
                    AuthorId = 1,
                    CreatedAt = DateTime.Now,
                    Decription = " Some",
                    Name="A project",
                    ProjectDeadline = DateTime.Now.AddDays(5), 
                    TeamId = 1

                },
                new Project{
                    Id = 2,
                    AuthorId = 2,
                    CreatedAt = DateTime.Now,
                    Decription = " Some",
                    Name="Another project",
                    ProjectDeadline = DateTime.Now.AddDays(5),
                    TeamId = 2

                }
            };

            var tasks = new List<Task>
            {
                new Task
                {
                    Id = 1,
                    CreatedAt = DateTime.Now,
                    FinishedAt = DateTime.Now.AddDays(4),
                    Description = "Some",
                    Name = "Just task",
                    PerfomerId = 1,
                    ProjectId = 1
                },
                new Task
                {
                Id = 2,
                CreatedAt = DateTime.Now,
                FinishedAt = DateTime.Now.AddDays(4),
                Description = "Some",
                Name = "Just another task",
                PerfomerId = 2,
                ProjectId = 2
                }
            };

            var teams = new List<Team>
            {
                new Team {
                    Id = 1,
                    CreatedAtDate = DateTime.Now,
                    Name = "Team 1"
                },
                new Team
                {
                    Id = 2,
                    CreatedAtDate = DateTime.Now,
                    Name = "Team 2"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = "Antony",
                    LastName = "Mac",
                    Birthday = DateTime.Parse("30.02.2002"),
                    Email = "someEmail@gmail.com",
                    RegisteredAt = DateTime.Parse("15.07.2021"),
                    TeamId = 1
                },
                new User
                {
                    Id = 2,
                    FirstName = "Maksym",
                    LastName = "Ant",
                    Birthday = DateTime.Parse("31.02.2003"),
                    Email = "some",
                    RegisteredAt = DateTime.Parse("16.07.2021"),
                    TeamId = 2
                }
            };

            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Task>().HasData(tasks);
            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);

            base.OnModelCreating(modelBuilder);
        }
    }
}
